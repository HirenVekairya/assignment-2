from autobahn.twisted.websocket import WebSocketServerProtocol, WebSocketServerFactory
import json
from twisted.internet import reactor
import sys
from twisted.python import log
from datetime import datetime


class MyServerProtocol(WebSocketServerProtocol):
    now = datetime.now()
    timestamp: float = datetime.timestamp(now)
    clients = {}
    groups = {}
    name = None
    group_name = None

    # this function will call when client try to connect with server
    def onConnect(self, request):
        print("Client connecting: {0}".format(request.peer))

    # this function  will call when  WebSocket connection will open.
    def onOpen(self):
        print("WebSocket connection open.")

    # this function will call when data will come form client side
    def onMessage(self, payload, isBinary):
        if not isBinary:
            print("")
            data = json.loads(payload.decode('utf8'))
            print("data from client received: ", data)
            self.handle_events(data['command'], data)

    # this function will call when client left from connection
    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: ", reason)
        if self.name in self.clients:
            del self.clients[self.name]

    # this function use for connect user with server
    def handle_login(self, kwargs):
        client = kwargs["username"].encode('utf-8')
        if client not in self.clients:
            self.name = client
            self.clients[client] = self
            kwargs["status"] = "Login successfully"
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)
        else:
            kwargs["status"] = " Sorry this username is already existing, Please use another username "
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for send messages
    def handle_message(self, kwargs):
        for name, protocol in self.clients.items():
            if protocol != self:
                username = self.name
                msg = {"command": kwargs['command'], "username": username.decode('utf8'), "message": kwargs['message'],
                       "Content-Type": "command/request", "status": "send successfully", "timestamp": self.timestamp}
                data_json = json.dumps(msg).encode('utf-8')
                protocol.sendMessage(data_json)

    # this function is used for creating group
    def create_group(self, kwargs):
        group_name = kwargs["group_name"].encode('utf-8')
        if group_name not in self.groups:
            self.group_name = group_name
            self.groups[group_name] = [self, ]
            kwargs["status"] = ' group created successfully'
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)
        else:
            kwargs["status"] = " Sorry this group name is already existing, Please use another group name "
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for join group
    def join_group(self, kwargs):
        group_name = kwargs["group_name"].encode('utf-8')
        if group_name in self.groups:
            if self not in self.groups[group_name]:
                self.groups[group_name].append(self)
                kwargs["status"] = 'group joined successfully'
                data_json = json.dumps(kwargs).encode('utf-8')
                self.sendMessage(data_json)
            else:
                kwargs["status"] = "you are already registered with this group"
                data_json = json.dumps(kwargs).encode('utf-8')
                self.sendMessage(data_json)
        else:
            kwargs["status"] = "Sorry this group name is not existing, Please use another group name "
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for left group
    def left_group(self, kwargs):
        group_name = kwargs["group_name"].encode('utf-8')
        if group_name in self.groups:
            if self in self.groups[group_name]:
                self.group_name = None
                self.groups[group_name].remove(self)
                kwargs["status"] = "you left successfully"
                data_json = json.dumps(kwargs).encode('utf-8')
                self.sendMessage(data_json)
            else:
                kwargs["status"] = "you are not registered with this group"
                data_json = json.dumps(kwargs).encode('utf-8')
                self.sendMessage(data_json)
        else:
            kwargs["status"] = " Sorry this group name is not existing, Please use another group name "
            data_json = json.dumps(kwargs).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for sending messages in group
    def group_message(self, kwargs):
        group_name = kwargs["group_name"].encode('utf-8')
        username = self.name
        if group_name in self.groups:
                for values in self.groups:
                    if values == group_name:
                        if self in self.groups[values]:
                            protocol = self.groups[values]
                            for j in protocol:
                                if j != self:
                                    msg = {"command": kwargs['command'], "group_name": group_name.decode('utf8'), "username": username.decode('utf8'), "group_message": kwargs['group_message'], "Content-Type": "command/request", "timestamp": self.timestamp}
                                    data_json = json.dumps(msg).encode('utf-8')
                                    j.sendMessage(data_json)
                        else:
                            msg = {"command": "group_name_error",
                                   "error": "you are not registered with this group",
                                   "group_name": group_name.decode('utf8'), "username": username.decode('utf8'),
                                   "Content-Type": "command/request", "timestamp": self.timestamp}
                            data_json = json.dumps(msg).encode('utf-8')
                            self.sendMessage(data_json)
        else:
            msg = {"command": "group_name_error", "error": "Sorry this group name is not existing, Please use another group name ", "group_name": group_name.decode('utf8'), "username": username.decode('utf8'), "Content-Type": "command/request",  "timestamp": self.timestamp}
            data_json = json.dumps(msg).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for making list of group
    def group_list(self):
        group_count = len(self.groups)
        single_or_plural = b'is' if group_count == 1 else b'are'
        group_or_groups = b'group' if group_count == 1 else b'groups'
        msg = {"command": "group_list", "group_count": group_count, "group_names": [],
               "single_or_plural": single_or_plural.decode('utf8'),
               "group_or_groups": group_or_groups.decode('utf8'), "Content-Type": "command/request",
               "timestamp": self.timestamp}
        for group_name in self.groups:
            msg["group_names"].append(group_name.decode('utf8'))
            data_json = json.dumps(msg).encode('utf-8')
            self.sendMessage(data_json)

    # # this function is used for making list of group members
    def group_member_list(self, kwargs):
        group_name = kwargs["group_name"].encode('utf-8')
        if group_name in self.groups:
            member_count = len(self.groups[group_name])
            single_or_plural = b'is' if member_count == 1 else b'are'
            person_or_people = b'member' if member_count == 1 else b'members'
            msg = {"command": "group_member_list", "group_name" : kwargs["group_name"],
                   "group_count": member_count, "group_member_list": [],
                   "single_or_plural": single_or_plural.decode('utf8'),
                   "person_or_people": person_or_people.decode('utf8'), "Content-Type": "command/request",
                   "timestamp": self.timestamp}
            for values in self.groups:
                if values == group_name:
                    if self in self.groups[values]:
                        protocol = self.groups[values]
                        for client, protocols in self.clients.items():
                            for i in protocol:
                                if i == protocols:
                                    msg["group_member_list"].append(client.decode('utf8'))
                                    data_json = json.dumps(msg).encode('utf-8')
                                    self.sendMessage(data_json)
                                    print(msg)
                    else:
                        msg = {"command": "group_name_error",
                               "error": "you are not registered with this group",
                               "group_name": group_name.decode('utf8'),
                               "Content-Type": "command/request", "timestamp": self.timestamp}
                        data_json = json.dumps(msg).encode('utf-8')
                        self.sendMessage(data_json)
        else:
            msg = {"command": "group_name_error",
                   "error": "Sorry this group name is not existing, Please use another group name ",
                   "group_name": group_name.decode('utf8'),
                   "Content-Type": "command/request", "timestamp": self.timestamp}
            data_json = json.dumps(msg).encode('utf-8')
            self.sendMessage(data_json)

    # this function is used for handle events
    def handle_events(self, args, kwargs):
        if args == 'login':
            self.handle_login(kwargs)
        elif args == 'send_message':
            self.handle_message(kwargs)
        elif args == 'create_group':
            self.create_group(kwargs)
        elif args == 'join_group':
            self.join_group(kwargs)
        elif args == 'left_group':
            self.left_group(kwargs)
        elif args == 'send_message_togroup':
            self.group_message(kwargs)
        elif args == 'group_list':
            self.group_list()
        elif args == 'group_member_list':
            self.group_member_list(kwargs)


if __name__ == '__main__':
    log.startLogging(sys.stdout)
    factory = WebSocketServerFactory()
    factory.protocol = MyServerProtocol
    reactor.listenTCP(8000, factory)
    reactor.run()
